import {Component, Injector, OnInit} from '@angular/core';
import {AuthService} from '@abp/ng.core';
import {OAuthService} from 'angular-oauth2-oidc';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private injector: Injector,
              private oAuthService: OAuthService,
              private authService: AuthService,
  ) {
    if (this.oAuthService.hasValidAccessToken() === false) {
      this.authService.init().then((res) => {
        this.authService.navigateToLogin();
        console.log('res', res);
      });
    } else {
    }
  }

  ngOnInit(): void {
  }

  logOut(): void {
    this.authService.logout();
  }
}
