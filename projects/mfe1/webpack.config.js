const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, '../../tsconfig.json'),
  [/* mapped paths to shared */]);

module.exports = {
  output: {
    uniqueName: "mfe1",
    publicPath: "auto"
  },
  optimization: {
    runtimeChunk: false
  },
  resolve: {
    // alias: {
    //   ...sharedMappings.getAliases(),
    // }
  },
  plugins: [
    new ModuleFederationPlugin({

      // For remotes (please adjust)
      name: "mfe1",
      filename: "remoteEntry.js",
      library: { type: "var", name: "mfe1" },
      exposes: {
        './Module': './projects/mfe1/src/app/flights/flights.module.ts',
        './ModuleMrViet': './projects/mfe1/src/app/mr-viet/mr-viet.module.ts',
      },

      shared: {
        "@angular/core": {singleton: true, strictVersion: true},
        "@angular/common": {singleton: true, strictVersion: true},
        "@angular/common/http": {singleton: true, strictVersion: true},
        "@angular/router": {singleton: true, strictVersion: true},

        ...sharedMappings.getDescriptors()
      }

    }),
    sharedMappings.getPlugin()
  ],
};
