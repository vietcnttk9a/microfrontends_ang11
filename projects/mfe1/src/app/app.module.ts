import {APP_INITIALIZER, Injector, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FlightsModule} from './flights/flights.module';
import {MrVietModule} from './mr-viet/mr-viet.module';
import {RouterModule} from '@angular/router';
import {APP_ROUTES} from './app.routes';
import {HomeComponent} from './home/home.component';
import {UrlProvider} from './shared/service-proxies/service-url-config/url-provider';
import {ServiceProxyModule} from './shared/service-proxies/service-proxy.module';
import {HttpClientModule} from '@angular/common/http';
import { PlatformLocation } from '@angular/common';
import { AppPreBootstrap } from 'shared/AppPreBootstrap';
import {CoreModule} from '@abp/ng.core';
import {environment} from '../../../shell/src/environments/environment';
import {registerLocale} from '@abp/ng.core/locale';
import {NgxsModule} from '@ngxs/store';
import {AppConsts} from './shared/AppConsts';

export function appInitializerFactory(injector: Injector, platformLocation: PlatformLocation) {
  return () => {
    return new Promise<boolean>((resolve, reject) => {
      AppPreBootstrap.run(injector, () => {
        resolve(true);
      });
    });
  };
}
const APPINIT_PROVIDES = [
  {
    provide: APP_INITIALIZER,
    useFactory: appInitializerFactory,
    deps: [Injector, PlatformLocation],
    multi: true,
  },
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CoreModule.forRoot({
      environment: AppConsts.abpEnvironment,
      registerLocaleFn: registerLocale(),
    }),
    NgxsModule.forRoot([], {developmentMode: !environment.production}),
    AppRoutingModule,
    FlightsModule,
    MrVietModule,
    ServiceProxyModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [UrlProvider, ...APPINIT_PROVIDES],
  bootstrap: [AppComponent]
})
export class AppModule {
}
