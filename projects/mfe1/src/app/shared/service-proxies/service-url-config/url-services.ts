import {AppConsts} from '../../AppConsts';

const AbpEnvironment = AppConsts.abpEnvironment;

export class UrlServices {
  static danhMucUrl(): string {
    return AbpEnvironment.apis.danhMuc.url;
  }
}

export const ApiNameConfig = {
  identity: {
    apiName: 'identity',
  },
  danhMuc: {
    apiName: 'danh-muc',
  },
  chuyenMon: {
    apiName: 'chuyen-mon',
  },
  khamChuaBenh: {
    apiName: 'kham-chua-benh',
  },
  portal: {
    apiName: 'portal',
  },
};
