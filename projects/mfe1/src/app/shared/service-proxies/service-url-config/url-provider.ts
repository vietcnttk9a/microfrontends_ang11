import {UrlServices} from './url-services';
import {API_BASE_URL as DANH_MUC_URL} from '../danh-muc-service-proxies';

export const UrlProvider = [
  {provide: DANH_MUC_URL, useFactory: UrlServices.danhMucUrl},
];
