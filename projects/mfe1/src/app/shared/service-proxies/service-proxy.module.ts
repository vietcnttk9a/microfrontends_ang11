import { NgModule } from '@angular/core';
import * as DanhMuc from './danh-muc-service-proxies';

const DanhMucServiceProxy = [
  DanhMuc.CommonServiceProxy,
  DanhMuc.DanhMucTinhServiceProxy,
  DanhMuc.DanhMucHuyenServiceProxy,
  DanhMuc.DanhMucXaServiceProxy,
  DanhMuc.DanhMucQuocTichServiceProxy,

  DanhMuc.DanhMucNhomDonViServiceProxy,
  DanhMuc.DanhMucDonViServiceProxy,
  DanhMuc.DanhMucDinhDuongServiceProxy,
  DanhMuc.DanhMucNhomDinhDuongServiceProxy,
  DanhMuc.FileServiceProxy,

  DanhMuc.NhomMonAnServiceProxy,
  DanhMuc.NhomThucPhamServiceProxy,
  DanhMuc.ChiSoBfaServiceProxy,
  DanhMuc.ChiSoHfaServiceProxy,
  DanhMuc.ChiSoWfaServiceProxy,
  DanhMuc.ChiSoWfhServiceProxy,
  DanhMuc.ThucPhamServiceProxy,
  DanhMuc.MonAnServiceProxy,
];


@NgModule({
  providers: [...DanhMucServiceProxy],
})
export class ServiceProxyModule {}
