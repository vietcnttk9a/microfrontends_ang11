import {Component, OnInit} from '@angular/core';
import {
  DanhMucDinhDuongDto,
  DanhMucDinhDuongServiceProxy,
  PagingDinhDuongRequest
} from '../../shared/service-proxies/danh-muc-service-proxies';

@Component({
  selector: 'app-my-test',
  templateUrl: './my-test.component.html',
  styleUrls: ['./my-test.component.css']
})
export class MyTestComponent implements OnInit {
  items: DanhMucDinhDuongDto[] = [];

  constructor(private ddService: DanhMucDinhDuongServiceProxy) {
  }

  ngOnInit(): void {
    const input = new PagingDinhDuongRequest();
    input.skipCount = 0;
    input.maxResultCount = 50;
    this.ddService.getlist(input).subscribe(res => {
      this.items = res.items;
    });
  }

}
