import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MrVietRoutingModule} from './mr-viet-routing.module';
import {MyTestComponent} from './my-test/my-test.component';
import {ServiceProxyModule} from '../shared/service-proxies/service-proxy.module';
import {UrlProvider} from '../shared/service-proxies/service-url-config/url-provider';


@NgModule({
  declarations: [
    MyTestComponent
  ],
  imports: [
    CommonModule,
    MrVietRoutingModule,
    ServiceProxyModule
  ],
  providers: [UrlProvider]
})
export class MrVietModule {
}
