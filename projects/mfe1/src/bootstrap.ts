import {enableProdMode, ViewEncapsulation} from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { environment } from './environments/environment';
import {AppConsts} from './app/shared/AppConsts';

if (environment.production) {
  enableProdMode();
}
fetch(`assets/${environment.appConfig}`)
  .then((res) => res.json())
  .then((json) => {
    AppConsts.abpEnvironment = json;
    AppConsts.abpEnvironment.production = environment.production;
    return import('./app/app.module').then((m) => m.AppModule);
  })
  .then((appModule) => {
    platformBrowserDynamic()
      .bootstrapModule(appModule, {
        defaultEncapsulation: ViewEncapsulation.Emulated,
        preserveWhitespaces: false,
      })
      .then((res) => {
        console.log('abpEnvironment', AppConsts.abpEnvironment);
        const win = window as any;
        if (win && win.appBootstrap) {
          win.appBootstrap();
        }
        return res;
      })
      .catch((err) => console.error(err));
  });
